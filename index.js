
class CookieManager {
    constructor(name , data) {
        this.name = name;
        this.data = data;
    };

    setCookie(x,y){
        this.name = x;
        this.data = y;
        var t = new Date();
        t.setTime(t.getTime() + (15*60*1000));
        document.cookie = this.name + "=" + this.data + ";" + "expires="+ t.toUTCString() + ";path=/";
    };

    getCookie(z) {
        this.name = z;
        var nameEQ = this.name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
    };

    clearCookie(n){
        this.name = n;
        document.cookie = this.name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    };

};

newCookie = new CookieManager();

/*
newCookie.setCookie('hello', 'world');
newCookie.getCookie('hello');
newCookie.clearCookie('hello');
*/