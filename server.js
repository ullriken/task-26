const express = require('express');
const app = express();
const path = require('path');
const cookieParser = require('cookie-parser');

// Expose files in www folder
app.use('/public', express.static(path.join(__dirname, 'www')));
app.use(cookieParser());
app.get('/index.js', function(req, res){
    res.sendFile(__dirname + '/index.js');
});

// Default Route for App
app.get('/', (req,res)=>{
    console.log('Hello Cookies!!');
    console.log(req.cookies);
    
    res.sendFile( path.join(__dirname, 'www', 'index.html' ) );
})

// Start listening on Port 3000
app.listen(3000, ()=> console.log('app has started'));

